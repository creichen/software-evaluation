#!/bin/sh

#
# Example setup using the aspectj compiler.
#
# IMPORTANT: Adjust the '*_HOME'-paths before running it from the
#            top-level directory of the project.
#

JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
ASPECTJ_HOME=~/opt/aspectj-1.9.2
ASPECTJ_JARS="$ASPECTJ_HOME/lib/aspectjtools.jar:$JAVA_HOME/lib/tools.jar"
SE_COMPILER="$JAVA_HOME/bin/java -cp $ASPECTJ_JARS -Xmx512M org.aspectj.tools.ajc.Main"
SE_ANT_RT=/usr/share/ant/lib/ant.jar

./bootstrap.py "$SE_COMPILER" -source 8 -target 8 -cp "$ASPECTJ_HOME/lib/aspectjrt.jar"
./build.py --compile fop               > compile.output.log
./build.py --run     fop -c SECallback > run.output.log


# This last command will fail if we do not use the SECallback
# when we run the benchmark. 
cat build/patch/dacapobench/build/benchmarks/bms.stats
