#!/bin/sh

#
# Example compile script which can be saved to a file 'build.sh'
# and placed in the top-level project directory.
#

# Set global compiler options (applies in every call to the compiler):
./bootstrap.py '$SE_COMPILER' -source 8 -target 8

# Compile named benchmarks using the specified compiler and configuration:
./build.py -DSE_COMPILER=/usr/lib/jvm/java-8-openjdk/bin/javac -DSE_ANT_RT=/usr/share/ant/lib/ant.jar --compile $@
