# WARNING

The instructions given here may at times be incorrect, but I will
try to keep them up to date.

The build.py script works with Python version 2.7.15rc1 but crashes with
version 3.7.2.

Is it a problem that we hardcode the shebang for all python scripts to
'!/usr/bin/python2'?

# Introduction
The goal of this project is to provide a framework which can be used for
source and binary analyses of software. The implementation provides a wrapper
around the benchmarks provided by `The Dacapo Benchmark Suite' (referred to as
the dacapobench) and instruments benchmark builds and dependencies to allow for
source code to be compiled with an arbitrary Java compiler. The jar file
resulting from compiling a benchmark is packaged using the method provided by
the dacapobench. The harness packaged with the jar is a slightly modified
version of the dacapobench harness, mainly to provide formatted output
conventient for our purposes.

This document explains how to build this project and transform, compile, and
execute the benchmarks embedded within the framework.

# Building the project
## System Requirements
The build for this project have been tested on a Ubuntu 18.04 desktop
distribution with a Python 2.7.15rc1 installation. Since this project
wraps around a multitude of software systems, each using it's own setup
for building and distributing artifacts, a number of additional software
systems are required in order to build benchmarks and dependencies included
in this project. Pre-requisites are: 

Python, Apache Ant, git, cvs (Concurrent Versions System), svn (subversion).

Note that a software system may download, install, and use additional
software during deployment as part of the build process in order to
distribute the system. An example of such software is Apache Maven.

## Building the project
A simple way to access the project source code is by cloning the
associated repository using git into a folder called 'software-evaluation':
```
git clone git@bitbucket.org:creichen/software-evaluation.git
```
Changing into the downloaded project directory ('software-evaluation'), we
find two python scripts at the top-level called 'build.py' and 'bootstrap.py'.
The project can be built by invoking 'build.py' from the command line with the
working directory set to the project directory (this is important in order for
the build script to be able to locate files during the build process.):
```
./build.py
```
This should automatically produce a directory called 'build' at the top-level
of the project directory and clone the dacapobench into it, extracting it's
contents. The 'build' directory will contain some subdirectories of importance
to the user, which we will come back to later.

The -c option can be used to "clean" the project:
```
./build.py -c
```
This option removes the 'build' directory produced by 'build.py' as mentioned
above. A goal of this project is that all software is downloaded once and cached
during the build process so that the framework is operational in offline mode;
cleaning the project clears the cache contained within the 'build' folder, which
means that you will have to run the build process and download everything again
to be able to continue to use the framework. During normal operation, the user
should not have to use the clean option.

The framework has built-in support for patching software. A user could take
advantage of this to instrument builds further. Read the section ``Extending
and patching the build'' in this document for an overview and instructions
for how to proceed.

# Building and executing benchmarks

Before a benchmark can be compiled it is neccessary to run the 'bootstrap.py'
script located in the top-level directory of the project. This script takes
two arguments. The first argument must be an absolute path to a java compiler
binary, such as javac, or, it could be an absolute path to any executable
program, as long as it is runnable from the command-line, additional arguments
are stored together with the binary in a generated wrapper script, which is
used by the build system to call the wrapped compiler. Call-site specific
arguments such as source files, output directory, and classpaths, are combined
with options given to 'bootstrap.py' before the compiler is invoked. This allows
each call-site and 'bootstrap.py' to contribute to the final set of options feed
to the wrapped compiler, which is necessary to free the build system of any
dependencies on specific compiler implementations. Implementation specific
arguments must therefore be given to 'bootstrap.py', and applies whenever the
compiler is invoked.

The compiler wrapper will expand environment and output the final command-line
on standard output before the compiler is invoked. If the wrapper finds an empty
argument at certain points during command-line processing, it will warn the user
that an environment variable may have expanded into an empty string. Keep an eye
out for that scenario should a build crash unexpectedly.

A call to 'bootstrap.py' could look like this:
```
./bootstrap.py '$SE_COMPILER' -verbose -source 8 -target 8 -cp $SE_CLASSPATH
```
where SE_COMPILER is assumed to be an environment variable set to the absolute
path of the compiler binary to be used, and SE_CLASSPATH --- a list of binaries
used by the compiler.

The build script will set environment variables before building a benchmark
if specified on the command-line using the -D option. The instrumentation for
certain libraries may require some special environment variables to be set to
compile. These can of course also be set manually, as usual, before compiling
invoking the build system.

As an example, assuming that we have already bootstrapped the compiler as shown
above, the fop benchmark can be compiled and executed by running:
```
./build.py -DSE_ANT_RT=/usr/share/ant/lib/ant.jar -DSE_COMPILER=/path/to/compiler --compile fop
```
followed by
```
./build.py --run fop
```
Since the instrumentation of the build system for fop requires access to the
ant runtime binaries through the environment variable SE_ANT_RT, we set the
variable accordingly.

Compiling a benchmark results in a jar-file with binaries and data for the user.
This archive can be found at the following path after compilation:
```
software-evaluation/build/patch/dacapobench/build/benchmarks/<bm-name>-bundle.jar
```
provided the top-level directory of the project was named 'software-evaluation'
as suggested earlier in this document. Of course, <bm-name> in this path is to
be substituted by 'fop' so that the archive filename becomes 'fop-bundle.jar'.

# Extending and patching the build

## Background

Throughout this section we will make some assumtions about the distribution of
software, and the responsibility of a build system during deployment. This
generalization, if you will, enable us to describe the framework's patching
process, which allows for build script and resource instrumentation, without
getting to much into system specific details.

Software is usually distributed and deployed in two steps: 1) physical distribution
of initial resources, including some kind of `build script', and 2) running the
included build script, deploying (installing) the system in full on a target machine.

It is not uncommon that the first step is repeated by the build system, in which case
the initial step can be seen as a bootstrap distribution containing a minimal set of
resource, allowing the build system to pull remaining (missing) artifacts from remote
locations over a network connection before installing the software. If the initial
distribution contains the complete set of resources required to build the project,
the build script may directly install the software onto the target machine without
further ado.

Truth be told, this process is not as linear as it seems, but for the purpose of this
discussion it will be accurate enough. It should also be mentioned that it is more or
less arbitrary by which method(s) a particular system is distributed and deployed, and
it is therefore difficult to address this topic in a way that generalizes to all projects.

The purpose of instrumenting a build script, used to deploy a specific software project,
is to gain more control over some aspect(s) of its deployment. For example, it may be
desirable to make sure that some resources are only downloaded once, by caching them on
the target system in a specific folder; if the system needs to be reinstalled (i.e. we go
through the deployment cycle again), the build script can reuse cached resources.
Such instrumentation, however, must be performed before the build script is invoked,
otherwise the actions we are trying to `redeem', remove, or `improve', may already be loaded,
and those we wanted to add will be to late to include.

Software is typically built on top of freestanding modules (libraries) providing specialized
and sometimes optimized functionality. These `dependencies' can be visualized using a dependency
graph. In order to build system A on a target machine, not only are the resources for system A
required, but also the resources for all systems that A depends on, implicitly and explicitly,
recursively, all the way down to independent modules, i.e. modules with no dependencies at all.
As a result, building (installing) system A also involves building all these additional modules,
which we not necessarily, by no means (as it should be), may see from the look of system A's
build script. Building a software system is therefore necessarily a recursive process where each
system's build script is responsible for distributing and deploying its own direct dependencies,
and invoke their respective build scripts, all which run independently without the knowledge of
higher-level extensions; it is necessary to allow this type of engineering. I should perhaps
point out that by `recursive' I'm appealing to the abstract notion of a build-method which is
implicitly specialized by each module (library) in form of a build script, of course, all of which
can be transformed and combined into an equivalent static procedure. Call conventions are managable
and not important, for the sake of argument.

But, now we are in trouble! How can we instrument a build script that we know nothing about, before
it is invoked `behind the scenes', in a chain of recursive calls, at an arbitrary depth, to effectively
control the deployment of the system we are interested in? Fortunately, we can do it, one step at a time.
Unfortunately, it is not going to be pretty. Following is a description of how. (Sorry, for the theatrics.)

## Instrumenting dependencies and caching resources

In progress, ...

Let's back up one step and walk through how a hypothetical build system might deal with a set of
dependencies and resources, and then present a method for how this work-flow can be modified to
allow us to caché downloaded resources, and to instrument dependencies.









 we could  
When all resources are present on the target machine, the build system goes ahead
and unpacks distributed archive files and installs software on the machine.


With this overview in mind, we will now focus more specifically on how a specific resource
tree should be treated within this framework to allow


In the following, we shall assume that all software projects use the same high-level
method for deploying and building their systems, namely, to 1)

 Distribute an initial
set of resource for the project, including some kind of build script which deploys
the software system on the targeted machine.
1. Download sources and other resources, if not contained in the distributed archive,
2. Unpack archives containing source trees into a 'build' folder,
3. Invoke an independent and autonomous subbuild (meaning that it is
   independent of whoever unpacks and calls it and only refers to paths
   relative and below the assigned 'build' folder, environment variables, and
   resources downloaded by it's own accord or accessible through prior setup. Exceptions can of course be made if
   they can be managed without breaking the patch protocol.)

(This applies to the whole project:
      Mention the downside that we patch against a specific version of a each
      individual project and so the patches are potentially only applicable to
      a specific (patched) version of a library. The amount of manual work
      required to gain control over the compilation process of a project
      depends on how well structured the build is.)

The framework can be used to extend the source trees of projects wrapped
around by 'build.py' and generate patches for these on a project-to-project
basis (actually even more fine-grained control can be established through some
hackery). The framework allows the user to develop patches iteratively either
from scratch or from the patches distributed by this project. Basically, if
changes are made to a specific version of the source tree rooted in a specific
predefined folder, the changes are captured automatically by the framework as
a patch when the 'build.py' script is invoked with the '-p' (patch) option.
The predefined folder is located at:
```
software-evaluation/build/patch
```
For this process to work correctly, the build systems responsible for
downloading and unpacking each source tree must be patched in the same manner
so that whenever a project is built, it's complete source tree and all it's
subprojects and dependencies source trees are unfolded and patched recursively,
i.e., to patch a subproject build we must first patch the parent project's build
preparing and calling the subproject's build. It is the parent build system's
responsibility to relocate the subproject's build folder to a subdirectory (direct descendant) of the predefined folder above in which all builds are rooted.
The name of the subdirectory must be unique and contain three copies of the
build folder of the project before the subbuild is invoked. These must be
named: 'build', build.modified, and 'build.original'.


Each build system is responsible for caching any downloaded resources in the
shared caché directory located at:
```
software-evaluation/build/dl
```



There is no general approach for building (including source compilation)
and deploying software. It is therefore neccessary to modify each build
individually and top-down to make it possible to capture source tree changes
as a patch. A method for modifying a project's source tree must neccessarily
be recursive since projects are often distributed as stand-alone modules or
extensions of previous modules in an evolutionary fashion. 

In order to take full control of the compilation process of a given benchmark
we are required to instrument each and every build system that is used directly
or indirectly to produce binaries required during execution of the benchmark.
For practical reasons it is also neccessary to modify the build such that
resources are only downloaded once.





The primary reason for instrumenting project builds is to take control of the production
process of binaries so that source and binary transformations can be applied to projects
source- and binary-files while still using the build system in place to compose
the project as intended by it's creators.



In order to take advantage of the built-in patching capabilities of the
framework all source trees must be extracted into a common predefined folder.
Every project source tree should have a folder with a unique name in this
predefined location containing three copies of the source tree nameed 'build',
'build.modified', and 'build.original', respectively. The 'build.modified' and
'build.original' folders will be used by the diff and patch utility to restore
and to produce and apply patches to the source tree; the 'build' folder will be
used for building the project. 

The patch method works as follows:
1. The build parent build system downloads a dependency source tree
   and unpack the source tree into a 'build' folder in a directory
   located in the predefined location. This folder can be named anything
   as long as the operating system plays along. The name is used as the
   name of the project as far as the framework is concerned.
2. The 'build' folder is then duplicated twice into folders named
   'build.original' and 'build.modified' and placed in the same folder as
   'build'.

Note: If the framework detected a saved patch for the project (identified by the
      folder name chosen above) this patch will be present under the name
      'build.patch' in the same folder as 'build'.

3. A patch is then produced by calling the 'make_patch.py' script
   available at SE_HOMe/bin/make_patch.py which overwrite the current patch
   for the project if the diff of 'build.original' and 'build.modified'
   produces output. The only argument to 'make_patch.py' is the project folder
   path given by '$SE_HOME/patch/<project name>' which contains the three build
   directories.

Note: The first time after unpacking the project the 

4. The 'build' and 'build.modified' directories are now patched by calling
   the 'apply_patch.py' script available at SE_HOMe/bin/apply_patch.py two
   times providing as first argument the absolute path of the project directory,
   namely, '$SE_HOME/patch/<project name>', and as second argument in the first
   call: 'build'; in the second call: 'build.modified'.

This also means that we are digging a deeper and deeper hole in the file tree
as we recursively descend into subprojects. We can solve this by flattening the
all builds into the same directory which we control by our build script. This
allows our build script to produce patches for any build wihtout invoking the
associated build system. The only thing we require from a subbuild is that its
build system produce three copies of the original source tree as distributed
by its creators and uses a task which we provide for creating and applying
patches to these copies. The user will also have to relocate the build and
distibution directory (dist) to the common build folder so that distributed
artifacts are easily accessible to our build. We also require that the builds
are patched to download resource once into a common resource folder so that
a user of our framework does not have to download sources time-and-time again.

One way of 

we can not count on that 
To modify the source tree of a specific dependency, the build system (parent
build) that invokes a sub-build must generate the sub-build 'build' directory
in the patch directory provided under this project's 'build' directory. This way
all project source trees are rooted in the same directory and the patch option
can produce build-patches to save user changes. This patch system depends on
that the user that patches a parent build to prepare for a subuild produces
three copies of the source tree upon the extraction of the subproject into the
patch directory located under this project's build directory.

the creates changes are flattened and are distributed into the 
for this project by
modifying source files in 'build.modified' directories found under the
top-level 'build' directory. The user can work iteratively to make changes
to system patches and use the built-in patch option ('-p') of 'build.py'
to save changes permanently:
```
./build.py -p
```
Patches are stored in the top-level 'patch' directory when saved with the
-p option. They will persist even if the project is cleaned.

Saving the current state of the build (using -p) will also deployed the saved patches directly to the build. and after the project is cleaned
and re-built. Cleaning the project without first saving the current state
of the build using the -p option will revert the build system patches to
the last saved version for each dependency.that last stored using 'build.modified' directories under 
The patch with the highest number is always applied when the project is
built from scratch or when the '-r' option is used to restore the build:
```
./build.py -r
```
A file can be added to the build or replace an existing version by placing
the file in a file hierarchy mirroring the 'build' directory in the 'include'
directory.

For example, if the file 'file.txt' is placed at 'include/file.txt' then the
file 'file.txt' will be copied to 'build/file.txt' when the project is built
and upon restoration.