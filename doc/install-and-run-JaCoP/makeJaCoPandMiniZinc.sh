#!/bin/bash

#
# Consider precompiling challenges to .fzn so that we do not have
# to build libminizinc in the framework.
#

# This script assumes that jacop.zip (MiniZinc Challenges 2018 data) is
# present in the current directory with the following structure:
#
# jacop.zip:
#   cargo/
#     ...
#   ...
#   mapping/
#     ...
#   ...

# Special requirements except for bash:
#     unzip,             (for unzipping MiniZinc Challanges 2018 archive)
#     git,               (for downloading JaCoP and libminizinc),
#     java               (for running JaCoP),
#     apache maven (mvn) (for building JaCoP),
#     cmake              (for building libminizinc),
#     c++ compiler       (for building libminizinc),

# Unzip pre-downloaded MiniZinc Challenge 2018 data bundle.
unzip jacop.zip
mv jacop jacop-data

# Clone JaCoP repository.
git clone https://github.com/radsz/jacop.git jacop-clone

# Build JaCoP --> jacop-<version>-SNAPSHOT.jar (<version>=4.7.0)
cd jacop-clone
mvn package -DskipTests=true
cd ..

# Clone libminizinc to build mzn2fzn compiler.
git clone https://github.com/MiniZinc/libminizinc.git libminizinc

# Build libminizinc --> mzn2fzn.
mkdir libminizinc/build
cd libminizinc/build
cmake ..
cmake --build .
cd ../..

# Add JaCoP definitions to standard library.
cp -r jacop-clone/src/main/minizinc/org/jacop/minizinc libminizinc/share/minizinc/jacop

cp jacop-clone/target/*.jar .

# Test if we can compile and run one of the challenges (time-out after 2 sec).

MZN2FZN=libminizinc/build/mzn2fzn           # mzn2fzn compiler binary.
MZN_STDLIB=libminizinc/share/minizinc       # Path to libraries.
MZN=jacop-data/cargo/cargo_coarsePiles      # Path to a mzn-file without .mzn extension.
DZN=jacop-data/cargo/challenge12_1422f_1644 # Path to a dzn-file without .dzn extension.

# Compile specified .mzn and .dzn files and assume the output .fzn file is
# named and placed at $MZN.fzn. 

"$MZN2FZN" --stdlib-dir "$MZN_STDLIB" -G jacop "$MZN".mzn -d "$DZN".dzn
java -cp jacop-4.7.0-SNAPSHOT.jar org.jacop.fz.Fz2jacop -v -t 2 "$MZN".fzn

# ./mzn2fzn --stdlib-dir $MZN_STDLIB -G jacop "$MZN".mzn -d "$DZN".dzn
# java -cp jacop-4.7.0-SNAPSHOT.jar org.jacop.fz.Fz2jacop -v -t 2 withjacop.fzn
