#!/bin/bash

MZN2FZN=libminizinc/build/mzn2fzn     # mzn2fzn compiler binary.
MZN_STDLIB=libminizinc/share/minizinc # Path to libraries.
MZN=jacop-data/mapping/mapping        # Path to a mzn-file without .mzn extension.
DZN=jacop-data/mapping/full2x2_mp3    # Path to a dzn-file without .dzn extension.

# Compile specified .mzn and .dzn files and assume the output .fzn file is
# named and placed at $MZN.fzn. 

"$MZN2FZN" --stdlib-dir "$MZN_STDLIB" -G jacop "$MZN".mzn -d "$DZN".dzn
java -cp jacop-4.7.0-SNAPSHOT.jar org.jacop.fz.Fz2jacop -v -t 10 "$MZN".fzn

# ./mzn2fzn --stdlib-dir $MZN_STDLIB -G jacop "$MZN".mzn -d "$DZN".dzn
# java -cp jacop-4.7.0-SNAPSHOT.jar org.jacop.fz.Fz2jacop -v -t 10 withjacop.fzn


