lucene         : nothing
derby          : junit,-Dapache.dl.url=https://archive.apache.org/dist
junit          : nothing
xerces         : -Dapache.dl.url=https://archive.apache.org/dist
janito         : nothing
h2             : nothing
commons-logging: nothing
commons-codec  : ${toolsdir}/apache-maven-3.5.2/bin/mvn
commons-httpclient: commons-logging,commons-codec

asm            : Error getting http://download.forge.objectweb.org/asm/asm-3.3-bin.zip to /home/zba10aoh/Documents/master_thesis/software-evaluation/dacapobench-clone/benchmarks/libs/asm/downloads/asm-3.3-bin.zip

bcel           : Error getting http://apache.wildit.net.au/jakarta/bcel/source/bcel-5.2-src.tar.gz to /home/zba10aoh/Documents/master_thesis/software-evaluation/dacapobench-clone/benchmarks/libs/bcel/downloads/bcel-5.2-src.tar.gz

daytrader      : ${build.time},-Dapache.dl.url=https://archive.apache.org/dist
dacapo-digest  : nothing
