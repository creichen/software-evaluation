#!/bin/bash

rm -rf build
mkdir build

cd build

# Clone JaCoP repository.
git clone https://github.com/radsz/jacop.git jacop-clone

# Build JaCoP --> jacop-<version>-SNAPSHOT.jar (<version>=4.7.0)
cd jacop-clone
mvn package -DskipTests=true
cd ..

# Extract JaCoP binaries.
cp jacop-clone/target/*.jar .

# Clone libminizinc to build mzn2fzn compiler.
git clone https://github.com/MiniZinc/libminizinc.git libminizinc

# Build libminizinc --> mzn2fzn.
mkdir libminizinc/build
cd libminizinc/build
cmake ..
cmake --build .
cd ../..

# Add JaCoP definitions to standard library.
cp -r jacop-clone/src/main/minizinc/org/jacop/minizinc libminizinc/share/minizinc/jacop

MZN2FZN=libminizinc/build/mzn2fzn     # mzn2fzn compiler binary.
MZN_STDLIB=libminizinc/share/minizinc # Path to libraries.

# Download MiniZinc Challenge 2018 data.
wget https://www.minizinc.org/challenge2018/mznc2018-probs.tar.gz

tar -zxf mznc2018-probs.tar.gz

# Convert selected minizinc problems into flatzinc.

SRC=mznc2018_probs
DEST=dat

function tofzn {
    MZN="$1/$2" # Path to a mzn-file without .mzn extension.
    DZN="$1/$3" # Path to a dzn-file without .dzn extension.
    
    mkdir -p "$DEST/$1"

    "$MZN2FZN" --stdlib-dir "$MZN_STDLIB" -G jacop "$SRC/$MZN".mzn -d "$SRC/$DZN".dzn
    cp "$SRC/$MZN".fzn "$DEST/$DZN".fzn
    
    java -Xss512M -Xmx2G -cp jacop-4.7.0-SNAPSHOT.jar org.jacop.fz.Fz2jacop -t 10 "$DEST/$DZN".fzn
}

rm -rf "$DEST"
mkdir "$DEST"

tofzn mapping mapping full2x2_mp3
tofzn oocsp_racks oocsp_racks oocsp_racks_050_r1
tofzn oocsp_racks oocsp_racks oocsp_racks_100_r1_cc
tofzn soccer-computational ecp xIGData_22_12_22_5
tofzn test-scheduling test-scheduling t30m10r10-5
tofzn test-scheduling test-scheduling t30m10r3-15

find "$DEST" -name "*.fzn" -printf "dat/%P\n" | zip dat -@
cd ..
cp build/dat.zip .


