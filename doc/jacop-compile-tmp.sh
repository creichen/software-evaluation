
# Execute this while standing in the top-level directory of JaCoP.
#
# This does resolve all dependenices except for the parser... which have to be generated?
#

# Required to build the parser.
mvn package -DskipTests

export JACOP_CLASSPATH=$(mvn -q exec:exec -Dexec.executable=echo -Dexec.args="%classpath")
echo ""
echo using classpath:
echo ""
echo $JACOP_CLASSPATH
echo ""
origin=$(pwd)
echo origin = "$origin"
cd src/main/java
rm -rf tmp
mkdir tmp
find . -name "*.java" > source-list.txt
javac -d tmp -cp $JACOP_CLASSPATH @source-list.txt
rm source-list.txt
cd "$origin"
