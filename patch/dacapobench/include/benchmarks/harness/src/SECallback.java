
import java.lang.Exception;
import java.io.BufferedWriter;
import java.io.FileWriter;

import org.dacapo.harness.Callback;
import org.dacapo.harness.CommandLineArgs;

public class SECallback extends Callback {

    private final String NEWLINE = System.getProperty("line.separator");
    private final String DELIM   = ",";
    
    private String bm_name;
    private long   duration;
    
    public SECallback(CommandLineArgs args) {
	super(args);
    }

    /* Immediately prior to start of the benchmark. */
    @Override
    public void start(String benchmark) {
	super.start(benchmark);
	this.bm_name = benchmark;
    };

    /* Immediately after the end of the benchmark. */
    @Override
    public void stop(long duration) {
	super.stop(duration);
	this.duration = duration;
    };

    @Override
    public void complete(String benchmark, boolean valid) {
	super.complete(benchmark, valid);

	/* Log duration in:
	       build/patch/dacapobench/build/benchmarks/bms.stats.
	*/

	String[] msg = new String[] {
	    this.bm_name,
	    (valid ? "PASSED" : "FAILED"),
	    "" + this.duration,
	    "msec",
	    (isWarmup() ? "y" : "n")
	};
	
	try (BufferedWriter bw =
                 new BufferedWriter(new FileWriter("bms.stats", true))) {
	    bw.append(String.join(DELIM, msg) + NEWLINE);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    };
}
