package org.dacapo.harness;

import java.io.File;
import java.lang.reflect.Constructor;

import java.nio.file.Files;
import static java.nio.file.StandardCopyOption.*;

import org.dacapo.harness.Benchmark;
import org.dacapo.parser.Config;

public class ExtendJ extends Benchmark {
    private final Object   benchmark;
    private       String[] args;

    public ExtendJ(Config config, File scratch) throws Exception {
        super(config, scratch);

        String[] args = config.preprocessArgs("default", scratch);

        if (args == null || args.length < 1)
            throw new Exception("Unspecified ExtendJ driver.");

        String driver = args[0]; /* e.g. org.dacapo.extendj.CompileBCEL */

        System.out.println("ExtendJ Benchmark Driver: " + driver);

        Class<?>       clazz  = Class.forName(driver, true, loader);
        this.method = clazz.getMethod("main", new Class[] { String[].class });
        Constructor<?> cons   = clazz.getConstructor();

        useBenchmarkClassLoader();
        try {
            benchmark = cons.newInstance();
        } finally {
            revertClassLoader();
        }

        /* Assume rest of arguments are arguments to
           org.extendj.JavaCompiler.main(String[] args). */

        for (int i = 0; i < args.length; ++i) {
            System.out.println("arg["+i+"]=" + args[i]);
        }

        int len = args.length;

        this.args = new String[len - 1];

        if (len > 1) {
            for (int i = 1; i < len; ++i) {
                this.args[i-1] = args[i];
            }
        }

        /* Copy rt.jar into scratch-directory. This path is used in
           extendj-*.cnf files and ends up in the 'args' array passed
           to the loaded benchmark driver in iterate(). */

        File rtSrcFile  = new File("../tools/java-8-openjdk-jre-rt.jar");
        File rtDestFile = new File("scratch/java-8-openjdk-jre-rt.jar");

        Files.copy(rtSrcFile.toPath(), rtDestFile.toPath(), REPLACE_EXISTING);
    }

    @Override
    protected void prepare(String size) throws Exception {
        super.prepare(size);
    }

    public void iterate(String size) throws Exception {
        method.invoke(benchmark, new Object[] { args });
    }
}

