package org.dacapo.jacop;

import org.jacop.fz.Fz2jacop;

public class Solve {
    private static final String[] challenges = new String[] {
	"scratch/jacop/mapping/full2x2_mp3.fzn",
	"scratch/jacop/oocsp_racks/oocsp_racks_050_r1.fzn",
	"scratch/jacop/oocsp_racks/oocsp_racks_100_r1_cc.fzn",
	"scratch/jacop/soccer-computational/xIGData_22_12_22_5.fzn",
	"scratch/jacop/test-scheduling/t30m10r10-5.fzn",
	"scratch/jacop/test-scheduling/t30m10r3-15.fzn"
    };
    
    public void main(String[] args) {

	/* Note: The benchmark driver must allocate an extra
	         slot for the challenge path in 'args'. */

	/* Note: Arguments are passed from jacop.cnf. */
	
	final int fileIndex = args.length - 1;

	for (String challenge: challenges) {

	    /* Solve challenge using JaCoP. */

	    args[fileIndex] = challenge;

	    Fz2jacop.main(args);
	}
    }
}
