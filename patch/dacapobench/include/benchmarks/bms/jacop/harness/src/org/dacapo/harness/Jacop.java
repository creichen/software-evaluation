package org.dacapo.harness;

import java.io.File;
import java.lang.reflect.Constructor;

import org.dacapo.harness.Benchmark;
import org.dacapo.parser.Config;

public class Jacop extends Benchmark {
  private final Object benchmark;
  private String[] args;

  public Jacop(Config config, File scratch) throws Exception {
    super(config, scratch);
    Class<?> clazz = Class.forName("org.dacapo.jacop.Solve", true, loader);
    this.method = clazz.getMethod("main", new Class[] { String[].class });
    Constructor<?> cons = clazz.getConstructor();
    useBenchmarkClassLoader();
    try {
	benchmark = cons.newInstance();
    } finally {
	revertClassLoader();
    }
  }

  @Override
  protected void prepare(String size) throws Exception {
    super.prepare(size);

    String[] tmp = config.preprocessArgs(size, scratch);
    int      len = (tmp != null) ? tmp.length : 0;

    /* One extra slot for path to challenge ("<folder>/<challenge>.fzn"). */
    
    this.args = new String[len + 1];

    for (int i = 0; i < len; ++i) {
	this.args[i] = tmp[i];
    }
  }

  public void iterate(String size) throws Exception {
    method.invoke(benchmark, new Object[] { args });
  }
}
