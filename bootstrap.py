#!/usr/bin/python2

# This script generates a compiler script with preset compiler binary and command-line options.
# Any non-standard options compiler options should be given as arguments to this script.
# The generated script will accept a list of standard javac command-line arguments.

import os
import subprocess
import sys

static_program_text =\
"""#!/usr/bin/python2

import os
import sys
import subprocess


def compile(L1,L2):

    print('[compiler.py] using command-lines:\\n\\t$(bootstrap)={},\\n\\t$(compiler) ={},'.format(L1,L2))

    global is_print_triggered

    # Whether warning should be displayed in print_warning() or not.
    is_print_triggered = True

    def print_warning(L1,L2):
        global is_print_triggered

        if is_print_triggered:

            # Disable further warnings.
            is_print_triggered = False

            # Note: We do not sys.exit(1) here since this may actually be the
            #       desired behaviour. We print a warning in case it was not.

            print('[compiler.py] warning using command-lines:\\n\\t$(bootstrap)={},\\n\\t$(compiler) ={},'.format(L1,L2))
            print('\\tan environment variable may have expanded into an empty string.')
            return

    def collect(options,L):
        while L:
            if not L[0]:
                print_warning(L1,L2)

            if L[0] and L[0][0] == '-':

                opt,L = L[0],L[1:]

                # Decide upon one flag to use for the classpath, namely: '-cp'.
 
                if opt == '-classpath':
                    opt = '-cp'

                if not L:
                    # Option without accompanying value (e.g. -verbose) last in list.
                    options[opt] = ''
                    break
                elif L[0] and L[0][0] == '-':
                    # Option without accompanying value (e.g. -verbose) in the middle of the list.
                    options[opt] = ''
                    continue;

                # Extract accompanying value.

                val,L = L[0],L[1:]

                if val[0] == '@':

                    # Load classpath from file.

                    pathfile = val[1:]

                    print('loading classpath from file:{}'.format(pathfile))

                    if os.path.exists(pathfile):
                        with open(pathfile,'r') as fp:
                            val = fp.read()

                    print('loaded classpath:{}'.format(val))

                # Merge options from different command lines (especially the classpath).

                if opt in options and opt == '-cp':

                    # Extend classpath.

                    options[opt] = options[opt] + ':' + val
                else:
                    options[opt] = val
            else:
                break

        return L # Rest

    compiler = L1[0]
    options  = {}
    print('[compiler.py] (Stage 1) Processing bootstrap command-line...')
    R        = collect(options,L1[1:])
    print('[compiler.py] (Stage 2) Processing compiler command-line...')
    R       += collect(options,L2)

    print('[compiler.py] (Stage 3) Processing sources and argfiles...')

    empty = [r for r in R if not r]

    if empty:
        print_warning(L1,L2)

    option_list = []
    for opt in options.keys():
        option_list.append(opt)
        val = options[opt]
        if val:
            option_list.append(options[opt])

    # Extract sources and argfiles from the tails of the command lines.
    # Note that it is allowed to specify option, sources, and argfiles
    # in both compiler stages. The only constraint is that the first
    # argument of in the first stage is a path to a java compiler binary.

    source    = [r for r in R if r and not r[0] == '@']
    argfiles  = [r for r in R if r and     r[0] == '@']

    cmd = [compiler] + option_list + source + argfiles

    # Expand environment variables by calling echo.

    print('[compiler.py] (Stage 4) Expanding environment variables...')

    process = subprocess.Popen(' '.join(['echo']+cmd),stdout=subprocess.PIPE,shell=True)
    (out,_) = process.communicate()

    out = out.strip()

    if not len(out.split(' ')) == len(cmd):
        print_warning(L1,L2)

    print('[compiler.py] (Stage 5) invoke: {}'.format(out))

    process = subprocess.Popen(out.split(' '))
    process.communicate()

"""

dynamic_program_text=\
"""

if __name__ == "__main__":
    argp = {}
    argq = sys.argv[1:]
    compile(argp,argq)

"""

def main():

    if len(sys.argv) < 2:
        msg =\
"""
At least one argument is required. The first argument must always be a
path to a java compiler binary accepting standard command-line options.
"""
        print(msg)
        sys.exit(1)

    print('[bootstrap.py] using first argument as binary={}'.format(sys.argv[1]))

    program_text = static_program_text + dynamic_program_text.format(sys.argv[1:])

    def write(path,text):
        if os.path.exists(path):

            filepath = os.sep.join([path,'compile.py'])

            with open(filepath,'w') as fp:
                fp.write(text)

            process = subprocess.Popen('chmod 755 {}'.format(filepath),shell=True)
            process.communicate()


    # Write compiler script to predefined locations.

    script    = os.sep.join(['bin'])
    build_bin = os.sep.join(['build','bin'])

    write(script,program_text)
    write(build_bin,program_text)


if __name__ == "__main__":
    main()

