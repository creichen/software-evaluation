#!/usr/bin/python2

import os
import sys
import subprocess

from include_files import include_files
from apply_patch   import apply_patch
from make_patch    import make_patch


def update(parent):
    '''
    Capture current state of 'build.modified' as a patch against
    'build.original' and then remove and restore 'build' and
    'build.modified' from 'build.original' applying the changes
    captured by the patch.
    '''

    print('[update.py] parent:{}'.format(parent))

    original = os.sep.join([parent,'build.original'])

    # Exit early if nothing to do.

    if not os.path.exists(original):
        print('[update.py] ... nothing to do.')
        return

    cwd = os.getcwd()

    os.chdir(parent)

    # Update patch locally in case the user made changes to the build.

    make_patch(parent)

    # Remove build directories.

    if os.path.exists('build'):
        process = subprocess.Popen("rm -r build",shell=True)
        process.communicate()

    if os.path.exists('build.modified'):
        process = subprocess.Popen("rm -r build.modified",shell=True)
        process.communicate()

    # Restore build directories.

    if os.path.exists('build.original'):
        process = subprocess.Popen("cp -r build.original build",shell=True)
        process.communicate()

        process = subprocess.Popen("cp -r build.original build.modified",shell=True)
        process.communicate()

        # Apply latest changes.

        apply_patch(parent,'build')
        apply_patch(parent,'build.modified')

        # We include files into 'build' not into 'build.modified'.

        include_files('include','build')

    os.chdir(cwd)

    print('[update.py] ... done.')


def main():
    args = sys.argv[1:]
    update(args[0])


if __name__ == '__main__':
    main()

