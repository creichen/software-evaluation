#!/usr/bin/python

import hashlib
import os
import subprocess
import sys

def hexdigest_text(text):
    ''' Return an MD5 hex-digest of the specified text. '''
    digest = hashlib.md5(text).hexdigest()
    return digest


def load_latest_patch(target):
    ''' Load and return the latest patch as a string. '''

    if not os.path.exists(target):
        return ''

    with open(target,'r') as fp:
        return fp.read()


def write_patch(target,patch):
    ''' Writes the specified text to the specified patch file path. '''

    with open(target,'w') as fp:
        fp.write(patch)


def make_patch(source):
    ''' Create a source tree patch. '''

    target   = os.sep.join([source,'build.patch'])
    original = os.sep.join([source,'build.original'])
    modified = os.sep.join([source,'build.modified'])

    # Generate a source tree patch by diffing 'build.original' and 'build.modified'.

    if os.path.exists(modified) and os.path.exists(original):

        print('creating patch for\n\tsource={}\n\ttarget={}'.format(source,target))

        # Change into the source directory so that we
        # can generate a system independent patch.

        cwd = os.getcwd()

        os.chdir(source)

        cmd     = ' '.join(['diff -aur','build.original','build.modified'])
        process = subprocess.Popen(cmd,stdout=subprocess.PIPE,shell=True)

        # According to the documentation we should not expect to be
        # able to pipe too large outputs from a subprocess:
        #
        #     https://docs.python.org/3/library/subprocess.html#subprocess.Popen.communicate
        #

        (current_patch,_) = process.communicate()

        if current_patch:

            # The build has been modified: Produce a patch
            # if changes differ from latest patch.

            latest_patch   = load_latest_patch(target)

            digest_latest  = hexdigest_text(latest_patch)
            digest_current = hexdigest_text(current_patch)

            if digest_latest != digest_current:
                write_patch(target,current_patch)

        os.chdir(cwd)

    else:
        print('failed to create patch for\n\tsource={}\n\ttarget={}'.format(source,target))
        print("\n\t[error] source directory does not contain 'build.{original,modified}' directories.")


def main():

    # Absolute path to parent directory containing 'build.original' and
    # 'build.modified' directories.

    source = sys.argv[1]

    make_patch(source)


if __name__ == '__main__':
    main()
