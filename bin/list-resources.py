#!/usr/bin/python2

import re
import os
import sys
import subprocess

# dir1/.../dirN/filename.ext --> header = dir1/.../dirN/filename.ext
# **/dir1/.../dirN/*.ext     --> header = *.ext
# dir1/.../dirN/**/*.ext     --> header = dir1/.../dirN, tail=**/*.ext
# dir1/.../dirN/**           --> header = dir1/.../dirN, tail=**

def main(args):
    
    def walk(path,patterns,allp):
        
        def word_matcher(word):
            '''
            Create a match object for the specified word, possibly containing wildcards (*).
            '''

            # print('word = {}'.format(word))
            ss   = word.split('*')
            nss  = len(ss)
            expr = []

            trailing_star = ss[-1] == ''
            
            # Handle first case.
    
            if not ss[0] and ss[1]:
                first = ss[1][0]
                first = first if not first in {'.'} else '\\' + first
                expr.append('[^{}]*'.format(first))
            
            for i in range(0,nss):
                if ss[i]:
                    # Append concrete text.
                    expr.append(ss[i])

                    if i < nss-1:
                        if ss[i+1]:
                            # Match any character except the first of the next concrete text.
                            first = ss[i+1][0]
                            first = first if not first in {'.'} else '\\' + first
                            expr.append('[^{}]*'.format(first))
                        else:
                            # Match any character at the end.
                            expr.append('[.]*')

            if not expr:
                # Match any non-empty sequence of characters.
                expr = ['[.]+']

            # Produce a match object for the constructed expression.

            e = '^' + ''.join(expr)

            if not trailing_star:
                e += '$'
            
            #print("regex:'{}'".format(e))
            
            return re.compile(e)


        def match(f,fpat):
            '''Check if filename 'f' match pattern 'fpat'.'''
            is_match = re.match(word_matcher(fpat),f)
            #print('[{}] match {} vs {}'.format(is_match,f,fpat))
            return is_match


        def collect(dirpath,fpats,allp):
            '''Collect files matching any pattern.'''

            #print('dirpath = {}'.format(dirpath))
            
            matches = []
            
            for f in os.listdir(dirpath):
                p = os.sep.join([dirpath,f])

                if os.path.isfile(p):
                    for fpat in fpats:
                        if allp or match(f,fpat):
                            matches.append(p)
                            print('found {}'.format(p))
                            break

            return matches


        # Include all files if a pattern includes everything.

        n = len(patterns)
        
        if not allp:
            if [p for p in patterns if len(p) == 1 and p[0] == '**']:
                patterns = []
                n        = 0
                allp     = True

        # Check files in current directory.

        matches  = collect(path,[''.join(p) for p in patterns if len(p) == 1], allp)
        matches += collect(path,[p[1] for p in patterns if len(p) == 2 and p[0] == '**'], allp)
        # Recurse into subdirectories that match a pattern.

        for f in os.listdir(path):

            pats  = [] # Patterns matching.

            # Determine all patterns which this directory match against and
            # propagate them into the next recursive call.

            if not os.path.isdir(os.sep.join([path,f])):
                # print('ignore file = {}'.format(f))
                continue
            
            if not allp:
                for i in range(0,n):

                    pat = patterns[i]

                    # print('test pattern = {}'.format(pat))
                    
                    if pat[0] == '**':
                        # Step past '**' if the next word matches 'f'.
                        # We always have at least two elements in the list.
                        if match(f,pat[1]):
                            pat = pat[2:]

                        pats.append(pat)
                        
                    elif match(f,pat[0]):
                        pats.append(pat[1:])
                    else:
                        #print('discard {}'.format(os.sep.join([path,f])))
                        pass

            # Recurse into subdirectory.

            if allp or pats:
                #print("recurse into '{}'".format(f))
                matches += walk(os.sep.join([path,f]),pats,allp);

        return matches


    # Collect files starting at basedir.

    basedir      = args[0]
    output_file  = args[1]
    patterns     = args[2]
    files        = args[3:]
    matches      = []
    
    # Split patterns into filenames (words).
    
    patterns = [p.split(os.sep) for p in patterns.split(',')]

    #print(patterns)
    
    for f in files:
        base = os.sep.join([basedir,f])
        #print('base = {}'.format(base))
        matches += walk(os.sep.join([basedir,f]),patterns,False)
    
    # Generate source list.

    with open(output_file,'w') as fp:
        for m in matches:
            fp.write(m)
            fp.write('\n')

if __name__ == '__main__':
    main(sys.argv[1:])
