#!/usr/bin/python

import os
import sys
import subprocess

def apply_patch(source,target):

    print('apply patch\n\tsource={}\n\ttarget={}'.format(source,target))

    target_dir = os.sep.join([source,target])
    patch_file = os.sep.join([source,'build.patch'])

    cmd = ' '.join(['patch -p1 <',patch_file])

    if os.path.exists(patch_file):
        if not os.path.exists(target_dir):
            print('[apply_patch.py] error: missing target: {}.'.format(target_dir))
            sys.exit(1)

        print('applying patch\n\tsource:{}\n\ttarget:{}'.format(source,target))
        
        cwd = os.getcwd()
        os.chdir(target_dir)
        process = subprocess.Popen(cmd,shell=True)
        process.communicate()
        os.chdir(cwd)

        print('patching... Done')


def main():
    # Absolute path to parent directory of build, build.modified, and build.original.
    source = sys.argv[1]

    # Should be 'build' or 'build.modified'
    target = sys.argv[2]

    apply_patch(source,target)


if __name__ == '__main__':
    main()
