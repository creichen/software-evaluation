#!/usr/bin/python

import os
import sys


def include_files(source,target):
    '''
    Add the files present in the source folder to the targeted folder.
    Warning: This action will overwrite targeted files by purpose. 
    '''

    if not os.path.exists(source) or not os.path.exists(target):
        print('[include_files.py] source or target folder does not exist.')
        return

    def include(src,tar):
        for f in os.listdir(src):

            source = os.sep.join([src,f])
            target = os.sep.join([tar,f])

            if os.path.isdir(source):
                if not os.path.exists(target):
                    os.mkdir(target)

                include(source,target)
            else:

                print('include {} --> {}'.format(source,target))

                with open(source,'r') as sfp:
                    with open(target,'w') as tfp:
                        tfp.write(sfp.read())

    print('including files...')
    include(source,target)
    print('including files... Done')


if __name__ == '__main__':

    source = sys.argv[1]
    target = sys.argv[2]

    include_files(source,target)
